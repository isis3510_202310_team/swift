//
//  ContentView.swift
//  Sportify
//
//  Created by Andres Felipe Delgado Ruiz on 23/03/23.
//

import SwiftUI
import Firebase


struct ContentView: View {
    
    @EnvironmentObject var sessionService: SessionServiceImpl
    
    var body: some View {

        NavigationView{
            switch sessionService.state {
            case .loggedIn:
                Main()
            case .loggedOut:
                SignInView()
            }
        }.environmentObject(sessionService)
        
    }
}

