
//  SignInView.swift
//  Sportify
//
//  Created by Andres Felipe Delgado Ruiz on 24/03/23.
//

import SwiftUI


struct SignInView: View {
    
    @StateObject private var vm = SignInViewModelImpl(service: LoginServiceImpl())
    
    @State var email = ""
    @State var password = ""
    
    @State private var errorMessage = ""
    var body: some View {
        
        VStack(spacing: 20){
            
            Image("logo")
                .renderingMode(.original)
                .resizable()
                .scaledToFit()
                .frame(width: 150, height: 150)
                .padding(.leading, 11.0)
            
            VStack{

                TextField("Email", text: $email)
                    .padding(8)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(mainColor, lineWidth: 2)
                    )
                    .padding(.top,4)
                    .keyboardType(.emailAddress)
                    
                
                
                SecureField("Password", text: $password)
                    .padding(8)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke(mainColor, lineWidth: 2)
                    )
                    .padding(.top,4)

                
                Text("Forgot Password?")
                    .foregroundColor(Color(red: 0.6039215686274509, green: 0.592156862745098, blue: 0.592156862745098))
                    .multilineTextAlignment(.trailing)
                    .padding(.top, 15)
                
                HStack{
                    Text("Don't have an account?")
                    NavigationLink("**Sign up**", destination: RegisterView())
                        .foregroundColor(mainColor)
                }
                .padding()
                
                Button(action : {
                    
                    vm.credentials.email = email
                    vm.credentials.password = password
                    vm.login()
                   
                }, label:{
                    Text("Log in")
                        .frame(maxWidth:.infinity)
                        .foregroundColor(.white)
                        
                })
                .buttonStyle(.borderedProminent)
                .tint(mainColor)
                
                if errorMessage != "" {
                  Text(errorMessage)
                        .foregroundColor(Color.red)
                }
            }
            .padding()
        }
        .frame(width: 350)
    }
}

