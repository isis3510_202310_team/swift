//
//  RegisterView.swift
//  Sportify
//
//  Created by Andres Felipe Delgado Ruiz on 24/03/23.
//

import SwiftUI
import Firebase
import FirebaseDatabase
import FirebaseStorage
import Foundation

struct RegisterView: View {
    
    @StateObject private var vm = RegistrationViewModelImpl(service: RegistrationServiceImpl())
       
    @State var fullName = ""
    @State var birthDate: Date = Date()
    @State var bio = ""
    @State var phoneNumber = ""
    @State var email = ""
    @State var password = ""
    
    // manage pick image or take picture
    @State var changeProfileImage = false
    @State var openCameraRoll = false
    @State var shouldPresentImagePicker = false
    @State var shouldPresentActionSheet = false
    @State var imageSelected = UIImage()
    @State private var errorMessage = ""
  
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            Button(action: {
                shouldPresentActionSheet = true
            }, label: {
                if changeProfileImage {
                    Image(uiImage: imageSelected)
                        .resizable()
                        .frame(width: 120, height: 120)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                } else {
                    Image("avatarIcon")
                        .resizable()
                        .frame(width: 120, height: 120)
                        .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
                }
            })
            
            Image(systemName: "camera")
                .frame(width: 30, height: 30)
                .foregroundColor(.white)
                .background(Color.gray)
                .clipShape(/*@START_MENU_TOKEN@*/Circle()/*@END_MENU_TOKEN@*/)
            
        }.sheet(isPresented: $shouldPresentImagePicker) {
            ImagePicker(selectedImage: $imageSelected, sourceType:  self.openCameraRoll ? .camera :  .photoLibrary)
        }.actionSheet(isPresented: $shouldPresentActionSheet) { () -> ActionSheet in
            ActionSheet(title: Text("Choose mode"), message: Text("Please choose your preferred mode to set your profile image"), buttons: [ActionSheet.Button.default(Text("Camera"), action: {
                self.changeProfileImage = true
                self.shouldPresentImagePicker = true
                self.openCameraRoll = true
            }), ActionSheet.Button.default(Text("Photo Library"), action: {
                self.changeProfileImage = true
                self.shouldPresentImagePicker = true
                self.openCameraRoll = false
            }), ActionSheet.Button.cancel()])
        }
        
    
        ScrollView{
            VStack(spacing: 12){
                
                VStack(spacing: 4){
                    Text("Full Name")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    TextField("", text: $fullName)
                        .textFieldStyle(.roundedBorder)
                }
                
                VStack(spacing: 4){
                    Text("Phone Number")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    TextField("", text: $phoneNumber)
                        .keyboardType(.numberPad)
                        .textFieldStyle(.roundedBorder)
                }
                
                VStack(spacing: 4){
                    Text("Birth Date")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    DatePicker("", selection: $birthDate, in: ...Date(),displayedComponents: .date)
                        .accentColor(mainColor)
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .padding(4)
                        .overlay(RoundedRectangle(cornerRadius: 5).stroke(Color.gray))
                        .cornerRadius(5)
                }
                
                
                VStack(spacing: 4){
                    Text("Email")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    TextField("", text: $email)
                        .textFieldStyle(.roundedBorder)
                        .keyboardType(.emailAddress)
                }
                
                VStack(spacing: 4){
                    Text("Password")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    SecureField("", text: $password)
                        .textFieldStyle(.roundedBorder)
                }
                
                VStack(spacing: 4){
                    Text("Bio")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    TextField("", text: $bio)
                        .textFieldStyle(.roundedBorder)
                }
            }
            .padding(.top, 60)
            .padding(.bottom, 40)
            
            Button(action :  {
                
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                
                vm.userDetails.name = fullName
                vm.userDetails.phoneNumber = Int(phoneNumber)
                vm.userDetails.birthDate = formatter.string(from: birthDate)
                vm.userDetails.email = email
                vm.userDetails.password = password
                vm.userDetails.bio = bio
                vm.userDetails.image = imageSelected
                
                vm.register()
                
            }, label:{
                Text("Register account")
                    .frame(maxWidth:.infinity)
                    .foregroundColor(.white)
            })
            .buttonStyle(.borderedProminent)
            .tint(mainColor)
            
        }
        .padding(.horizontal, 30)
        
        
        if errorMessage != "" {
            Text(errorMessage)
                .foregroundColor(Color.red)
        }
    }
}




