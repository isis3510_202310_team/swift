//
//  MainView.swift
//  Sportify
//
//  Created by MacBookPro on 24/03/23.
//

import SwiftUI

enum Tab {
    case home
    case calendar
    case add
    case group
    case profile
}

struct Main: View {
    
    @State private var selectionTab: Tab = .home
    
    var body: some View {
        TabView(selection: $selectionTab){
            Home().environmentObject(ReadEvents())
                .tabItem {
                    Label("Home", systemImage: "house")
                }
                .tag(Tab.home)

            CalendarView()
                .tabItem {
                    Label("Calendar", systemImage: "calendar")
                }
                .tag(Tab.calendar)
            
            CreateEvent(selectionTab: $selectionTab)
                .environmentObject(ModelData())
                .environmentObject(MapItemViewModel())
                .environmentObject(SportViewModel())
                .tabItem{
                    Label("Add",systemImage: "plus.app")
                }
                .tag(Tab.add)
            
            Group()
                .tabItem{
                    Label("Group",systemImage: "person.and.person.fill")
                }
            
            Profile()
                .environmentObject(ModelData())
                .environmentObject(SportViewModel())
                .tabItem{
                    Label("Profile",systemImage: "person")
                }
                .tag(Tab.profile)
        }.accentColor(mainColor)
    }
}

struct Main_Previews: PreviewProvider {
    static var previews: some View {
        Main()
    }
}
