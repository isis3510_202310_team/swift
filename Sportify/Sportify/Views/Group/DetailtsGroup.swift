//
//  DetailtsGroup.swift
//  Sportify
//
//  Created by Diegoe012 on 30/05/23.
//

import SwiftUI
import Firebase

struct DetailtsGroup: View {
    
    @State private var messageText = ""
    var messages: [Message]
    var nameGroup: String
    var image: String
    
    var body: some View {
        
        VStack{
            ScrollView{
                ForEach(messages){ message in
                    MessageItem( message: message)
                }
            }.padding(.horizontal, 10)
            
            Spacer()
            Divider()
            HStack {
                TextField("Enter your message", text: $messageText)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding(.leading, 20)
                    .shadow(color: .gray, radius: 2)

                Button(action: {
                    messageText = ""
                    print("Message sent")
                }) {
                    
                    Image(systemName: "play.fill")
                        .foregroundColor(mainColor)
                        .font(.system(size: 30))
                }
                .padding(10)
                .padding(.trailing, 10)

            }
            .padding(.bottom, 4)
        }
        .toolbar{
            HStack{
                CachedImage(item: (name: nameGroup, url: image), animation: .spring(),transition: .slide.combined(with: .opacity)) { phase in
                    switch phase {
                    case .empty:
                        ProgressView()
                            .frame(width: 40, height: 40)
                    case .success(let image):
                        image
                            .resizable()
                            .frame(width: 40, height: 40)
                            .clipShape(Circle())
                    case .failure(_):
                        Image(systemName: "xmark")
                            .symbolVariant(.circle.fill)
                            .foregroundStyle(.white)
                            .frame(width: 40, height: 40)
                            .background(mainColor, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
                    @unknown default:
                        fatalError()
                    }
                }
                .padding(.leading)
                .padding(.bottom)
                
                Spacer()
                
                Text(nameGroup)
                    .font(.title2)
                    .bold()
                    .padding(.trailing )
            }
        }
    }
}

