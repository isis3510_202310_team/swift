//
//  GroupView.swift
//  Sportify
//
//  Created by MacBookPro on 24/03/23.
//

import SwiftUI
import Network



struct Group: View {
    
    @EnvironmentObject var networkMonitor: NetworkMonitor
    @StateObject private var groupViewModel = GroupViewModel()
    @State private var path = NavigationPath()
 
    @State private var isPanelVisible = false
  

    var body: some View {
        
        
        NavigationStack(path: $path){
            ScrollView{
                VStack(spacing: 8){
                    ForEach(groupViewModel.groupList){ group in
                        GroupItem(sportGroup: group, path: $path)
                    }
                }
                .sheet(isPresented: $isPanelVisible) {
                    NewGroup(isPanelVisible: $isPanelVisible)
                }
            }
            .navigationTitle("Groups")
            .toolbar{
                ToolbarItem(placement: .navigationBarTrailing){
                    Button(action: {
                        isPanelVisible = true
                    }) {
                        Image(systemName: "plus.app.fill")
                            .font(.system(size: 20))
                            .foregroundColor(mainColor)
                    }
                }
            }
//            .navigationDestination(for: String.self){ nameView in
//
//                if(nameView == "Details Group"){
//                }
//            }
            
        }
     }
}
