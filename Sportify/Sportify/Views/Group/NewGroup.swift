//
//  NewGroup.swift
//  Sportify
//
//  Created by Diegoe012 on 29/05/23.
//

import SwiftUI
import PhotosUI

struct NewGroup: View {
    
    @Binding var isPanelVisible: Bool
    @State private var groupName = ""
    @State private var groupDescription = ""
    
    @State private var selectedItem : [PhotosPickerItem] = []
    @State private var groupImage: UIImage?
    @State private var idGroup: UUID = UUID()
    
    let repo: RepositoryInterface = Repository()
    
    var body: some View {
        VStack {
            Text("New Group")
                .font(.title)
                .frame(maxWidth: .infinity, alignment: .leading)
            
            ScrollView{
    
                if let groupImage{
                    Image(uiImage: groupImage)
                        .resizable()
                        .scaledToFit()
                        .clipShape(RoundedRectangle(cornerRadius: 10))
                        .frame(width: 350)
                }
                
                PhotosPicker(
                    selection:$selectedItem,
                    maxSelectionCount: 1,
                    matching: .images
                ){
                    Text("Pick Photo Event")
                        .foregroundColor(mainColor)
                        .padding()
                }.onChange(of: selectedItem){newItem in
                    Task{
                        do{
                            if let data = try? await newItem.first?.loadTransferable(type: Data.self){
                                groupImage = UIImage(data: data)
                            }
                        }
                    }
                }
                
                
                VStack(spacing: 3){
                    Text("Group Name")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    TextField("The coolest Name", text: $groupName , axis: .vertical)
                        .textFieldStyle(.roundedBorder)
                }
                
                VStack(spacing: 3){
                    Text("Group Description")
                        .font(.subheadline)
                        .foregroundColor(mainColor)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    TextField("A greate description" , text: $groupDescription, axis:  .vertical)
                        .textFieldStyle(.roundedBorder)
                        .lineLimit(3...10)
                }
            }
            .padding(.bottom, 20)
            
            
            Button(action :  {
                isPanelVisible = false
                createGroup()
                
            }, label:{
                Text("Create Group")
                    .frame(maxWidth:.infinity)
                    .foregroundColor(.white)
            })
            .buttonStyle(.borderedProminent)
            .tint(mainColor)
            
            Spacer()
        }
        .padding()
    }
    
    func createGroup(){
        Task{
            repo.uploadImageEvent(groupImage, idGroup){ downloadUrlImage in
                guard let imageUrl = downloadUrlImage else {
                    return
                }
                repo.writeNewGroup(idGroup, groupName, groupDescription, imageUrl){ success in
                    print("Group Upload")
                    groupName = ""
                    groupDescription = ""
                    groupImage = nil
                }
            }
        }
    }
}
