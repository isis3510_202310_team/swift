//
//  Message.swift
//  Sportify
//
//  Created by Diegoe012 on 30/05/23.
//

import SwiftUI
import Firebase

struct MessageItem: View {
    
    var message: Message
    
    
    var body: some View {
        if !(message.userId == Auth.auth().currentUser?.uid ?? " " ) {
            HStack(spacing: 0){
                
                CachedImage(item: (name: "test", url: "https://firebasestorage.googleapis.com:443/v0/b/sportify-2c499.appspot.com/o/events%2F2DB7929E-48F4-4679-A218-24AF926A8F03.jpg?alt=media&token=40547751-3b3f-402c-b950-1102c25d651f"), animation: .spring(),transition: .slide.combined(with: .opacity)) { phase in
                    switch phase {
                    case .empty:
                        ProgressView()
                            .frame(width: 40, height: 40)
                    case .success(let image):
                        image
                            .resizable()
                            .frame(width: 40, height: 40)
                            .clipShape(Circle())
                    case .failure(_):
                        Image(systemName: "xmark")
                            .symbolVariant(.circle.fill)
                            .foregroundStyle(.white)
                            .frame(width: 40, height: 40)
                            .background(mainColor, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
                    @unknown default:
                        fatalError()
                    }
                }
                
                VStack(alignment: .leading) {
                    Text(message.message)
                        .padding(10)
                        .background(.white)
                        .foregroundColor(.black)
                        .cornerRadius(10)
                        .shadow(color: .gray, radius: 2, x: 0, y: 2)
                    
                    HStack {
                        Text(message.hour ?? "")
                            .font(.caption)
                            .foregroundColor(.gray)
                        
                        Spacer()
                        
                        Text(message.date ?? "")
                            .font(.caption)
                            .foregroundColor(.gray)
                    }
                }
                .padding(.horizontal)
                .padding(.vertical, 5)
            }

            
        }else {
            VStack(alignment: .trailing) {
                Text(message.message)
                    .padding(10)
                    .background(secondColor)
                    .foregroundColor(.black)
                    .cornerRadius(10)
                
                
                HStack {
                    Text(message.hour ?? "")
                        .font(.caption)
                        .foregroundColor(.gray)
                    
                    Spacer()
                    
                    Text(message.date ?? "")
                        .font(.caption)
                        .foregroundColor(.gray)
                }
            }
            .padding(.horizontal)
            .padding(.vertical, 5)
        }
                
    }
}

struct Message_Previews: PreviewProvider {
    static var previews: some View {
        MessageItem(message: Message(id: 1, userId: "1", message: "Hello world", hour: "10:30 AM", date: "May 30"))
    }
}
