//
//  GroupItem.swift
//  Sportify
//
//  Created by Diegoe012 on 20/05/23.
//

import SwiftUI

struct GroupItem: View {
    
    var sportGroup: sportGroup
    
    @Binding var path: NavigationPath
    
    var body: some View {
        
        HStack{
            CachedImage(item: (name: sportGroup.name, url: sportGroup.image), animation: .spring(),transition: .slide.combined(with: .opacity)) { phase in
                switch phase {
                case .empty:
                    ProgressView()
                        .frame(width: 150, height: 150)
                case .success(let image):
                    image
                        .resizable()
                        .frame(width: 150, height: 150)
                case .failure(_):
                    Image(systemName: "xmark")
                        .symbolVariant(.circle.fill)
                        .foregroundStyle(.white)
                        .frame(width: 150, height: 150)
                        .background(mainColor, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
                @unknown default:
                    fatalError()
                }
            }
            
            VStack(alignment: .leading,spacing: 4){
                Text(sportGroup.name)
                    .font(.title3)
                    .padding(.top, 4)
                Divider()
                
                Text("\(sportGroup.description)")
                    .font(.system(size: 11))
                    .padding(.horizontal, 10)
                    .multilineTextAlignment(.center)
                Divider()
                
                
                HStack{
                    NavigationLink {
                        DetailtsGroup(messages: [Message(id: 1, message: "dsdsa")], nameGroup: "Miga", image: "")
                    } label: {
                        Text("Details")
                            .frame(maxWidth:.infinity)
                            .foregroundColor(.white)
                            .padding(.horizontal, 10)
                            .padding(.vertical, 5)
                            .background(mainColor)
                            .cornerRadius(10)
                    }
                    
                }
                
            }.padding(.horizontal, 10)
            
        }
        .frame(height: 150)
        .background(Color.purple.opacity(0.2))
        .clipShape(RoundedRectangle(cornerRadius: 10))
        .padding(15)
    }
}


