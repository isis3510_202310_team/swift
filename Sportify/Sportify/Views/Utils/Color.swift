//
//  Color.swift
//  Sportify
//
//  Created by MacBookPro on 1/05/23.
//

import Foundation
import SwiftUI

let mainColor = Color(red: 0.403921568627451, green: 0.3137254901960784, blue: 0.6431372549019608)
let secondColor = Color.purple.opacity(0.2)
let thirdColor = Color.purple.opacity(0.5)

