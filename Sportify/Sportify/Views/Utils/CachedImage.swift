//
//  CachedImage.swift
//  Sportify
//
//  Created by Diegoe012 on 21/05/23.
//

import SwiftUI

struct CachedImage<Content: View>: View {
    
    @StateObject private var manager = CachedImageManager()
    let item: (name:String, url: String)
    let animation: Animation?
    let transition: AnyTransition
    let content: (AsyncImagePhase) -> Content
    
    init(
        item: (name:String, url:String),
        animation: Animation? = nil,
        transition: AnyTransition = .identity,
        @ViewBuilder content: @escaping (AsyncImagePhase) -> Content) {
            self.item = item
            self.animation = animation
            self.transition = transition
            self.content = content
    }
    
    
    var body: some View {
        ZStack{
            switch manager.currentState {
            case .loading:
                content(.empty)
                    .transition(transition)
            case .success(let data):
                if let image = UIImage(data: data){
                    content(.success(Image(uiImage: image)))
                        .transition(transition)
                }else{
                    content(.failure(CachedImageError.invalidData))
                        .transition(transition)
                }
            case .failed(let error):
                content(.failure(error))
                    .transition(transition)
            default:
                content(.empty)
                    .transition(transition)
            }
            
        }
        .animation(animation, value: manager.currentState)
        .task {
            await manager.load(item)
        }
    }
}



extension CachedImage {
    enum CachedImageError: Error {
        case invalidData
    }
}
