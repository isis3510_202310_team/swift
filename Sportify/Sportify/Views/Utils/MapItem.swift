//
//  Map.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import MapKit
import SwiftUI

struct flag: Identifiable{
    let id = UUID()
    let name :String
    let coordinate: CLLocationCoordinate2D
}

struct MapItem: View {
    
    @StateObject var mapViewModel = MapItemViewModel()
    @EnvironmentObject private var modelData: ModelData

    
    var body: some View {
        VStack {
            
//            Text("lat: \(mapViewModel.region.center.latitude), long: \(mapViewModel.region.center.longitude). Zoom: \(mapViewModel.region.span.latitudeDelta)")
//            .font(.caption)
//            .padding()
            
            Map(coordinateRegion: $mapViewModel.region,
                interactionModes: .all,
                showsUserLocation: true,
                annotationItems: [flag(name: modelData.eventName, coordinate: CLLocationCoordinate2D(
                    latitude: mapViewModel.region.center.latitude,
                    longitude: mapViewModel.region.center.longitude)
                )]
            ){
                MapMarker(coordinate: $0.coordinate)
            }
            .onAppear{
                DispatchQueue.main.async {
                    mapViewModel.checkIfLocationServicesIsEnabled()
                }
            }.onTapGesture {
                location in
                print("Tapped: ",location)
            }
            .onChange(of:mapViewModel.region.center.latitude){newLatitude in
                DispatchQueue.main.async {
                    modelData.latitude = Float(newLatitude)
                }
                
            }
            .onChange(of: mapViewModel.region.center.longitude){
                newLongitude in
                DispatchQueue.main.async {
                    modelData.longitude = Float(newLongitude)
                }
            }
        }
    }
}
