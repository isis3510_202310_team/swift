//
//  SportItem.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import SwiftUI

struct SportItem: View {
    
    @EnvironmentObject var modelData: ModelData
    var sport: Sport
    var isSelected: Bool
    
    var backgroundColor: Color {
        if isSelected {
            return mainColor
        }
        else{
            return Color(red: 103, green: 80, blue: 164)
        }
    }
    
    var fontColor: Color{
        if isSelected{
            return .white
        }
        else{
            return .black
        }
    }
    
    var body: some View {
        HStack{
            Text(sport.emoji)
            Text("  "+sport.name)
                .bold()
        }.font(.system(size: 14))
        .padding(8)
        .background(backgroundColor)
        .foregroundColor(fontColor)
        .cornerRadius(15)
        .shadow(color: Color.black, radius: 3)
        
    }
}

struct SportItem_Previews: PreviewProvider {
    
    static var modelData = ModelData()
    
    static var previews: some View {
        SportItem(sport: modelData.sports[6],isSelected: false)
            .environmentObject(modelData)
        
    }
}
