//
//  NotConnection.swift
//  Sportify
//
//  Created by MacBookPro on 1/05/23.
//

import SwiftUI

struct NotConnection: View {
    var body: some View {
        VStack(spacing:16){
            Image(systemName:"wifi.slash")
                .resizable()
                .scaledToFit()
                .foregroundColor(mainColor)
                .frame(width: 64, height: 64)
                    
            Text("Network Connection")
                .font(.title)
                .fontWeight(.bold)
            
            Text("Seems to be offline.")
                .font(.headline)
                .foregroundColor(.gray)
            
            Text("Please check your connectivity")
                .font(.headline)
                .foregroundColor(.gray)
        }
    }
}

struct NotConnection_Previews: PreviewProvider {
    static var previews: some View {
        NotConnection()
    }
}
