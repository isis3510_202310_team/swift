//
//  ProfileView.swift
//  Sportify
//
//  Created by MacBookPro on 24/03/23.
//

import SwiftUI
import FirebaseAuth

struct Profile: View {
    
    @EnvironmentObject var sessionService: SessionServiceImpl
    @EnvironmentObject var modelData: ModelData
    @EnvironmentObject var networkMonitor: NetworkMonitor
    @State private var isExpanded = false
   
    
    var body: some View {
        VStack(spacing: 4){
            
            CachedImage(item: (name: "avatar", url: sessionService.userDetails?.avatar ?? "" ), animation: .spring(),transition: .slide.combined(with: .opacity)) { phase in
                switch phase {
                case .empty:
                    ProgressView()
                        .frame(width: 150, height: 150)
                case .success(let image):
                    image
                        .resizable()
                        .frame(width: 130, height: 130) // Set your desired size
                        .clipShape(Circle()) // Clip the image into a circle shape
                        .overlay(Circle().stroke(Color.black, lineWidth: 1)) // Add a border to the circle
                        .shadow(radius: 4) // Add a
                        .padding(.top, 25)
                    
                case .failure(_):
                    Image(systemName: "xmark")
                        .symbolVariant(.circle.fill)
                        .foregroundStyle(.white)
                        .frame(width: 150, height: 150)
                        .background(mainColor, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
                @unknown default:
                    fatalError()
                }
            }
            
            if networkMonitor.isConnectedFireBase && networkMonitor.isConnected {
                
                Text((sessionService.userDetails?.name ?? "N/A"))
                    .font(.title) // Set the desired font
                    .multilineTextAlignment(.center) // Align text in the center
                    .padding(.vertical, 15)
                
                Text((sessionService.userDetails?.bio ?? "N/A"))
                    .font(.system(size: 15)) // Set the desired font
                    .multilineTextAlignment(.center) // Align text in the center
                
                if(sessionService.userDetails?.interests.count == 0){
                    
                    NavigationLink(destination: SelectSportInterests().environmentObject(modelData)
                    ) {
                        Text("Add your interests..")
                            .frame(width: 230 ,height: 32)
                            .foregroundColor(.white)
                            .background(mainColor)
                            .cornerRadius(10)
                    }
                    .padding(.top, 20)
                    .padding(.bottom, 27)
                    DisclosureGroup(
                        isExpanded: $isExpanded,
                        
                        content: {
                            Text("Interests not selected yet")
                                .font(.system(size: 14))
                        },
                        label: {
                            Text("Interests")
                                .font(.system(size: 14))
                            
                            
                        }
                    ).frame(width: 240)
                        .padding(.bottom, 15)
                    
                    
                    
                    
                }else{
                    NavigationLink(destination: SelectSportInterests().environmentObject(modelData)
                    ) {
                        Text("Add more interests..")
                            .frame(width: 230 ,height: 32)
                            .foregroundColor(.white)
                            .background(mainColor)
                            .cornerRadius(10)
                    }
                    .padding(.top, 20)
                    .padding(.bottom, 27)
                    
                    
                    DisclosureGroup(
                        isExpanded: $isExpanded,
                        
                        content: {
                            Text(sessionService.userDetails?.interests.joined(separator: ", ") ?? "")
                                .padding()
                        },
                        label: {
                            Text("Interests")
                                .font(.system(size: 14))
                        }
                    ).frame(width: 240)
                        .padding(.bottom, 15)
                    
                    
                    
                    
                }
                
                
                List {
                    Section {
                        Text("Birth Date : \(sessionService.userDetails?.birthDate ?? "N/A")")
                            .padding(.vertical,10)
                        Text("Email : \(sessionService.userDetails?.email ?? "N/A")")
                    }
                    Section {
                        Button("Settings"){
                        }
                        Button("Statistics"){
                        }
                        Button("Logout"){
                            sessionService.logout()
                        }
                    }
                    
                }
            }else{
                
                Text("User Profile")
                    .font(.title) // Set the desired font
                    .multilineTextAlignment(.center) // Align text in the center
                    .padding(.vertical, 15)
                Image("no-wifi")
                    .resizable()
                    .frame( width:150,height: 150)
                    .padding(.vertical,20)
                Text("No internet connection, please check your network")
                    .frame(width: 240)
                    .padding(.vertical, 15)
                List {
                    
                   
                    Section {
                        
                        Button("Logout"){
                            sessionService.logout()
                        }
                    }
                    
                }
            }
            
        }
        .navigationTitle("Profile")
        .alert(isPresented: .constant(!networkMonitor.isConnected)) {
            Alert(
                title: Text("No Internet Connection"),
                message: Text("Please check your network settings and try again."),
                dismissButton: .default(Text("OK"))
            )
        }
        
    }
 
    
    
}


