//
//  SelectSport.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import SwiftUI
import WrappingStack

struct SelectSportInterests: View {
    
    
    @EnvironmentObject var modelData: ModelData
    @StateObject private var sportViewModel = SportViewModel()
    let repo: RepositoryInterface = Repository()
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var networkMonitor: NetworkMonitor
    
    var isValid: Bool{
        modelData.selectedSports.isEmpty
    }
    
    
    var body: some View {
        VStack{
            Text("Which sports do you want to include?")
                .padding(.bottom,15)
            WrappingHStack {
                ForEach(sportViewModel.sportsList){
                    sport in
                    SportItem(sport: sport, isSelected: modelData.selectedSports.contains(sport))
                        .padding(4)
                        .contentShape(Rectangle())
                        .onTapGesture {
                            withAnimation(.easeInOut(duration: 0.1)){
                                modelData.selectedSports.formSymmetricDifference([sport])
                            }
                        }
                }
            }
            Text("The selected sports will override your previous interests")
                .padding(.top,15)
            
            if !networkMonitor.isConnected {
                Text("No internet connection, action disabled")
                    .padding(.top,15)
                
            }
            
            Button(action : {
                AddInterests()
                self.presentationMode.wrappedValue.dismiss()
            }, label:{
                Text("Add")
                    .frame(maxWidth: 260)
                    .foregroundColor(.white)
                    
            })
            .buttonStyle(.borderedProminent)
            .tint(mainColor)
            .padding(.top, 100)
            .disabled(!networkMonitor.isConnected)
            
        }
        .navigationTitle("Select Sports")
    }
    
    func AddInterests(){
        Task{
                repo.updateUserInterests(modelData){ sucesss in
                    modelData.clean()
                }
            }
        }
    }



