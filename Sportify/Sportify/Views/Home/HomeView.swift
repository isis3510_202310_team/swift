//
//  HomeView.swift
//  Sportify
//
//  Created by MacBookPro on 24/03/23.
//

import SwiftUI

struct Home: View {
    var body: some View {
        EventListView()
    }
}

struct Home_Previews: PreviewProvider {
    static var previews: some View {
        Home()
    }
}
