//
//  EventListView.swift
//  Sportify
//
//  Created by Lina Sierra on 25/03/23.
//

import Foundation
import SwiftUI
import Firebase
import FirebaseDatabase
import CoreLocation



struct EventListView: View {
    let locationManager = LocationManager()
    let sports = ["Tennis", "Boxing", "Basketball", "Volleyball", "Football", "Golf", "Ping Pong", "Rugby", "Taekwondo", "Bicycle"]
    let imageFolder = "Sports"
    let images = ["tenis", "boxing", "basketball", "volleyball", "soccer", "Golf", "Ping Pong", "Rugby", "Taekwondo", "Bicycle" ]
    var eventos: [Event] = []
    @State private var selectedSport: String = ""
    // datos de base de datos
    
    
    
    // Aquí declaras el texto de búsqueda
    @State private var searchText = ""
    @State private var filtro = false
    @State private var closer = false
    @State var showCarousel = true
    @StateObject var viewModel = ReadEvents();
    func isClose(latitud: Double, longitud: Double) -> Bool {
        let lat1 = latitud
        let lon1 = longitud
        guard let lat2 = locationManager.currentLocation?.coordinate.latitude,
              let lon2 = locationManager.currentLocation?.coordinate.longitude
        else {
            return false // No se puede obtener la ubicación actual
        }
        let earthRadiusKm = 6371.0 // Radio de la tierra en kilómetros
        let dLat = toRadians(number: (lat2 - lat1))
        let dLon = toRadians(number: (lon2 - lon1))
        let lat1Radians = toRadians(number: lat1)
        let lat2Radians = toRadians(number: lat2)

        let a = pow(sin(dLat/2), 2) + cos(lat1Radians) * cos(lat2Radians) * pow(sin(dLon/2), 2)
        let c = 2 * atan2(sqrt(a), sqrt(1-a))
        let distanceKm = earthRadiusKm * c
        let distanceMeters = distanceKm * 1000
        return distanceMeters <= 1000.0
        
        
    }
    
    func hasSport(sportsList: [String], sportGiven: String) -> Bool
    {
        if (sportsList.contains(sportGiven)){
            return true
        }
        else {return false}
    }
    
    func toRadians(number: Double) -> Double {
            return number * .pi / 180.0
        }
    
    var body: some View {
        NavigationView {
            VStack {
                if (selectedSport == ""){
                    SearchBarWithPicker(text: $searchText, filtro: $filtro, closer: $closer, showCarousel: $showCarousel, viewModel: viewModel)}
                if showCarousel && !closer && !filtro{
                    CarouselView(images: images, sports: sports, selectedSport: $selectedSport)
                        .frame(height: 280)
                        .padding(.horizontal)
                }

                
                List {
                    var eventos = filtro ? viewModel.eventsFilter : viewModel.events
                        let filteredEvents = eventos.filter({ searchText.isEmpty ? true : $0.name.localizedStandardContains(searchText)})
                    ForEach(filteredEvents, id: \.name) {
                        event in
                        if (!closer || (closer && isClose(latitud: event.latitude!, longitud: event.longitude!))) && (selectedSport == "" || (selectedSport != "" && hasSport(sportsList: event.sports!, sportGiven: selectedSport ))) {
                        VStack{
                            HStack {
                                Text(event.name)
                                Spacer()
                                Button(action: {}) {
                                    Text("Details")
                                        .foregroundColor(.purple)
                                        .padding(.horizontal, 10)
                                        .padding(.vertical, 5)
                                        .background(Color.purple.opacity(0.2))
                                        .cornerRadius(10)
                                }
                                .buttonStyle(PlainButtonStyle())
                            }
                            
                            CachedImage(item:(name: event.name ?? "", url: event.img ?? "" ), animation: .spring(),transition: .slide.combined(with: .opacity)) { phase in
                                switch phase {
                                case .empty:
                                    ProgressView()
                                        .frame(width: 300, height: 300)
                                case .success(let image):
                                    image
                                        .resizable()
                                        .frame(width: 300, height: 300)
                                case .failure(_):
                                    Image(systemName: "xmark")
                                        .symbolVariant(.circle.fill)
                                        .foregroundStyle(.white)
                                        .frame(width: 300, height: 300)
                                        .background(mainColor, in: RoundedRectangle(cornerRadius: 8, style: .continuous))
                                @unknown default:
                                    fatalError()
                                }
                            }
                            
                            
                            
                        }
                    }
                        }
                }
                
                
            }
        }
    }
    
    
}
struct SearchBarWithPicker: View {
    @Binding var text: String
    @Binding var filtro: Bool
    @Binding var closer: Bool
    @State var selectedCategory: String = ""
    @Binding var showCarousel: Bool
    
    let categories = [ "Filter", "This week", "This month", "Closer events"]
    @StateObject var viewModel: ReadEvents
    
    var body: some View {
        HStack {
            
            TextField("What do you want to do?", text: $text, onEditingChanged: { editing in
                if editing {
                    showCarousel = false
                } else {
                    showCarousel = true
                }
            })
                .padding(.horizontal, 10)
                .frame(height: 50)
                .background(Color.purple.opacity(0.2))
                .cornerRadius(10)
                .overlay(
                    HStack {
                        Spacer()
                        HStack{
                            Picker(selection: $selectedCategory, label: Text("")) {
                                ForEach(categories, id: \.self) { category in
                                    Text(selectedCategory == category ? category.replacingOccurrences(of: "(This | events)", with: "", options: .regularExpression) : category)
                                }
                            }
                            .accentColor(Color.purple)
                            .frame(width: 95, height: 25)
                            .background(Color.white)
                            .cornerRadius(5)
                            .onChange(of: selectedCategory){
                                newValue in
                                switch selectedCategory {
                                    case "This week":
                                        viewModel.filterEvents(tipo: "week")
                                        filtro = true
                                    case "This month":
                                        viewModel.filterEvents(tipo: "month")
                                        filtro = true
                                    case "Closer events":
                                        closer = true
                                    default:
                                        filtro = false
                                        closer = false
                                    }
                            }
                        }
                        HStack{
                            Image(systemName: "magnifyingglass")
                                .foregroundColor(.purple)
                                .padding(.trailing, 10)
                        }
                        
                        
                    }
                )

            Button(action: {
                self.text = ""
                showCarousel = true
            }) {
                Image(systemName: "xmark.circle.fill")
                    .foregroundColor(.purple)
            }
            .padding(.trailing, 10)
            
        }
        .padding(.vertical, 10)
        .padding(.bottom, 10)
        .padding(.leading, 15)
    }
    
    
       
}
struct CarouselView: View {
    let images: [String]
    let sports: [String]
    @Binding var selectedSport: String

    var body: some View {
        VStack(alignment: .leading) {
            Text("Choose your sport:")
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 20) {
                    ForEach(images.indices, id: \.self) { index in
                                            VStack {
                                                Image(images[index])
                                                    .resizable()
                                                    .scaledToFill()
                                                    .frame(width: 300, height: 200)
                                                    .cornerRadius(10)
                                                    .shadow(radius: 5)
                                                    .onTapGesture {
                                                        if selectedSport == "" {selectedSport = sports[index]}
                                                        else {selectedSport = "" }
                                                                            }
                                                VStack {
                                                    Text(sports[index])
                                                        .font(.headline)
                                                        .foregroundColor(.primary)
                                                    Spacer()
                                                }
                                                .padding(.horizontal)
                                            }.background(selectedSport == sports[index] ? Color.purple.opacity(0.2) : Color.clear)
                            .cornerRadius(10)
                                        }
                }
            }
        }
    }
}
struct EventListView_Previews: PreviewProvider {
    static var previews: some View {
        EventListView()
    }
}
