//
//  SelectSport.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import SwiftUI
import WrappingStack

struct SelectSport: View {
    
    
    @EnvironmentObject var modelData: ModelData
    @StateObject private var sportViewModel = SportViewModel()
    
    @Binding var selectionTab: Tab
    @Binding var path: NavigationPath
    
    var isValid: Bool{
        modelData.selectedSports.isEmpty
    }
    
    
    var body: some View {
        VStack{
            Text("Which sports do you want to include?")
            WrappingHStack {
                ForEach(sportViewModel.sportsList){
                    sport in
                    SportItem(sport: sport, isSelected: modelData.selectedSports.contains(sport))
                        .padding(4)
                        .contentShape(Rectangle())
                        .onTapGesture {
                            withAnimation(.easeInOut(duration: 0.1)){
                                modelData.selectedSports.formSymmetricDifference([sport])
                            }
                        }
                }
            }
        }
        .navigationTitle("Select Sports")
        .toolbar{
            ToolbarItem(placement: .navigationBarTrailing){
                NavigationLink("Next", value: "Data Picker").disabled(isValid)
            }
        }
    }
}


