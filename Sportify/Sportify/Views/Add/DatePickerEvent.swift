//
//  datePicker.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import SwiftUI

struct DatePickerEvent: View {
    
    @EnvironmentObject var modelData: ModelData
    
    @Binding var selectionTab: Tab
    @Binding var path: NavigationPath
    
    var body: some View {
        Form{
            Section(header: Text("When is the event?")){
                DatePicker(
                        "Event Date",
                        selection: $modelData.dateEvent,
                        in: Date()...,
                        displayedComponents: [.date]
                    )
                    .datePickerStyle(.graphical)
            }
            Section(header: Text("At what time is it? ")){
                DatePicker(
                        "Event Hour",
                        selection: $modelData.hourEvent,
                        displayedComponents: [.hourAndMinute]
                    )
                    .datePickerStyle(.graphical)
                
            }
            
        }
        .navigationTitle("Select Date Event")
        .toolbar{
            ToolbarItem(placement: .navigationBarTrailing){
                NavigationLink("Next", value: "Add Details")
            }
        }
        
    }
}
