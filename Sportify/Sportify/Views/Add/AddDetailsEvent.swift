//
//  AddDetailsEvent.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import SwiftUI
import Firebase
import FirebaseStorage
import PhotosUI

struct AddDetailsEvent: View {
    
    @EnvironmentObject  var modelData: ModelData
    @State private var idEvent: UUID = UUID()
    
    @Binding var selectionTab: Tab
    @Binding var path: NavigationPath
    
    @State private var showingAlert = false
    
    @State private var selectedItem : [PhotosPickerItem] = []
    @State private var eventImage: UIImage?
    
    let repo: RepositoryInterface = Repository()
    
    var body: some View {
        Text(modelData.eventName).bold().padding()
        
        if let eventImage{
            Image(uiImage: eventImage)
                .resizable()
                .scaledToFit()
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .frame(width: 350)
        }
        
        Form{    
            PhotosPicker(
                selection:$selectedItem,
                maxSelectionCount: 1,
                matching: .images
            ){
                Text("Pick Photo Event")
            }.onChange(of: selectedItem){newItem in
                Task{
                    do{
                        if let data = try? await newItem.first?.loadTransferable(type: Data.self){
                            eventImage = UIImage(data: data)
                        }
                    }
                }
            }
    
            Section(header: Text("Details Event")){
                TextField("Details...", text: $modelData.descriptionEvent)
                    .padding(.vertical, 40)
                
                Button(action: {
                    CreateEvent()
                    path = NavigationPath()
                    showingAlert = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        selectionTab = .home
                    }
                    
                }, label: {
                    Text("Create")
                        .frame(maxWidth: .infinity)
                })
                .alert(isPresented: $showingAlert){
                    Alert(
                        title: Text("Event Created"),
                        message: Text("Wear sunscreen!"),
                        dismissButton: .default(Text("Got it!"),action: {
                            withAnimation{
                                selectionTab = .home
                            }
                        })
                    )
                }
                .buttonStyle(.borderedProminent)
                .tint(mainColor)
            }
        }.navigationTitle("Create Event")
    }
    
    func CreateEvent(){
        Task{
            repo.uploadImageEvent(eventImage,idEvent){ downloadUrlImage in
                guard let imageUrl = downloadUrlImage else {
                    return
                }
                modelData.imageUrl = imageUrl
                repo.writeNewEvent(modelData,idEvent){ sucesss in
                    modelData.clean()
                    eventImage = nil
                }
            }
        }
    }
}
