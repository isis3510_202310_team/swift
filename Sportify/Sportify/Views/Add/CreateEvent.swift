//
//  CreateEventView.swift
//  Sportify
//
//  Created by MacBookPro on 24/03/23.
//

import SwiftUI
import MapKit




struct CreateEvent: View {
    
    @Binding var selectionTab: Tab
    @EnvironmentObject var modelData: ModelData
    @EnvironmentObject var networkMonitor: NetworkMonitor
    @State private var showNetworkAlert = false
    @State private var path = NavigationPath()
    @State private var showMap = true
        
    private var isValid:Bool{
        !(!modelData.eventName.isEmpty && modelData.eventName.count > 3 )
    }
    
            
    var body: some View {
        if networkMonitor.isConnectedFireBase && networkMonitor.isConnected || !showMap{
            
            NavigationStack(path: $path){
                VStack{
                    Text("Name Event")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal)
                    
                    TextField("Micro Tournament",text: $modelData.eventName)
                        .padding(.horizontal)
                        .textFieldStyle(.roundedBorder)
                    
                    Text("Where does the event take place: ")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding(.horizontal)
                    
                    MapItem()
                }
                .navigationTitle("Create Event")
                .toolbar{
                    ToolbarItem(placement: .navigationBarTrailing){
                        NavigationLink("Next", value: "Select Sport").disabled(isValid)
                    }
                }
                .navigationDestination(for: String.self){ nameView in
                    
                    if(nameView == "Select Sport"){
                        SelectSport(selectionTab: $selectionTab, path: $path)
                    }
                    
                    if(nameView == "Data Picker"){
                        DatePickerEvent(selectionTab: $selectionTab, path: $path)
                    }
                    
                    if(nameView == "Add Details"){
                        AddDetailsEvent(selectionTab: $selectionTab, path: $path)
                    }
                }
            }
            
        }else{
            NotConnection()
        }
        
    }
}

