//
//  sport.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import Foundation

struct Sport: Hashable, Codable, Identifiable{
    var id: Int
    var name: String
    var isActive: Bool
    var emoji: String
    var img: String
    var type: String
}
