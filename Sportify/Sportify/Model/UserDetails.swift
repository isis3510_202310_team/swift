//
//  UserDetails.swift
//  Sportify
//
//  Created by Diegoe012 on 23/05/23.
//

import Foundation

struct UserDetails {
    let name: String
    let bio: String
    let birthDate: String
    let email: String
    let avatar: String
    let phone: Int
    let interests: [String]
    
}
