//
//  LoginCredentials.swift
//  Sportify
//
//  Created by Diegoe012 on 23/05/23.
//

import Foundation

struct LoginCredentials {
    var email: String
    var password: String
}

extension LoginCredentials {
    static var new: LoginCredentials {
        LoginCredentials(email: "", password: "")
    }
}
