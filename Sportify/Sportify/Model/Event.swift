//
//  Event.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import Foundation

struct Event: Hashable, Codable{
    var name: String
    var description: String?
    var date: String?
    var startTime: String?
    var endTime: String?
    var idUser: String?
    var img: String?
    var sports: [String]?
    var latitude: Double?
    var longitude: Double?
}
