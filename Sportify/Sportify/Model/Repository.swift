//
//  FirebaseRepository.swift
//  Sportify
//
//  Created by MacBookPro on 25/04/23.
//

import Foundation
import Firebase
import FirebaseStorage

protocol RepositoryInterface{

    func getAllSportsFromDataBase(completion: @escaping ([Sport]) -> Void)
    func getAllEventsFromDataBase(completion: @escaping ([Event]) -> Void)
    func getAllGroupsFromDataBase(completion: @escaping ([sportGroup]) -> Void)
    func uploadImageEvent(_ eventImage: UIImage?,_ idEvent: UUID, completion: @escaping (String?) -> Void)
    func uploadImageUser(_ avatarImage: UIImage?,_ idUser: String, completion: @escaping (String?) -> Void)
    func writeNewEvent<T: ModelData>(_ modelData: T,_ idEvent: UUID, completion: @escaping (String?) -> Void)
    func writeNewGroup(_ idGroup: UUID, _ name: String,_ description: String, _ img: String, completion: @escaping (String?) -> Void)
    func updateUserInterests<T: ModelData>(_ modelData: T, completion: @escaping (String?) -> Void)
}


final class Repository: RepositoryInterface{
    
    func getAllSportsFromDataBase(completion: @escaping ([Sport]) -> Void) {
        getAllSports(){ sportsList in
            completion(sportsList)
        }
    }
    
    func getAllEventsFromDataBase(completion: @escaping ([Event]) -> Void) {
        getAllEvents(){ events in
            completion(events)
        }
    }
    
    func getAllGroupsFromDataBase(completion: @escaping ([sportGroup]) -> Void) {
        getAllGroups(){ groups in
            completion(groups)
        }
    }
    
    func uploadImageEvent(_ eventImage: UIImage?,_ idEvent: UUID, completion: @escaping (String?) -> Void){
        uploadImageToFireBase(eventImage, idEvent){ link in
            completion(link)
        }
    }
    
    func uploadImageUser(_ avatarImage: UIImage?,_ idUser: String, completion: @escaping (String?) -> Void){
        uploadAvatarImageToFireBase(avatarImage, idUser){ link in
            completion(link)
        }
    }
    
    
    func writeNewEvent<T: ModelData>(_ modelData: T,_ idEvent: UUID, completion: @escaping (String?) -> Void){
        if true{
            writeNewEventDataBase(modelData,idEvent){ successs in
                completion(successs)
            }
        }
    }
    
    func writeNewGroup(_ idGroup: UUID, _ name: String, _ description: String, _ img: String, completion: @escaping (String?) -> Void){
        writeNewGroupDataBase(idGroup, name, description, img){ success in
            completion(success)
        }
    }
    
    func updateUserInterests<T: ModelData>(_ modelData: T, completion: @escaping (String?) -> Void){
        if true{
            updateUserInterestsDataBase(modelData){ successs in
                completion(successs)
            }
        }
        
    }
    
}
