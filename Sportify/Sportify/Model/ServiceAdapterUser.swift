//
//  ServiceAdapterUser.swift
//  Sportify
//
//  Created by Andres Felipe Delgado Ruiz on 28/04/23.
//

import Foundation
import Firebase
import FirebaseStorage

func uploadAvatarImageToFireBase(_ avatarImage: UIImage?,_ idUser: String, completion: @escaping (String?) -> Void) {
    
         
    let storageRef = Storage.storage().reference()
    
    let imageData = avatarImage?.jpegData(compressionQuality: 0.8)
    
    guard imageData != nil else{
        return
    }
    
    
    let fileRef = storageRef.child("avatars/\(idUser).jpg")
    
    _ = fileRef.putData(imageData!, metadata: nil){metadata, error in
        guard let metadata = metadata else{
            debugPrint("ErrorUploadAvatarImage",error?.localizedDescription ?? "")
            completion(nil)
            return
        }
        
        _ = metadata.size
        
        fileRef.downloadURL(){ (url, error) in
            guard let downloadUrlImage = url else {
                debugPrint("errorDownloadUrlImage:",error?.localizedDescription ?? "")
                completion(nil)
              return
            }
            completion(downloadUrlImage.description)
        }
    }
    
}

func updateUserInterestsDataBase<T: ModelData>(_ modelData: T, completion: @escaping (String?) -> Void) {
    let eventsRef = Database.database().reference().child("users")
    do {
       
        
        let sports = modelData.selectedSports.map {$0.name}
        let userId = Auth.auth().currentUser?.uid
        
        let interests = [
            "interests": sports,
        ] as [String : Any]
        
        
        eventsRef.child(userId!).updateChildValues(interests){ (error: Error?, ref:DatabaseReference) in
            if let error = error{
                debugPrint("Data could not be saved: \(error).")
                completion(nil)
            }else{
                debugPrint("Data saved successfully!")
                completion("Success")
            }
        }
    }catch let error {
        debugPrint("ErrorCreateEvent",error.localizedDescription)
    }
}
