//
//  Message.swift
//  Sportify
//
//  Created by Diegoe012 on 30/05/23.
//

import Foundation

struct Message: Hashable, Codable, Identifiable {
    var id: Int
    var userId: String?
    var message: String
    var hour: String?
    var date: String?
}

