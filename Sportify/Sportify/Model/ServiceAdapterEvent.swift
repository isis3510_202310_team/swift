//
//  ServiceAdapterEvent.swift
//  Sportify
//
//  Created by MacBookPro on 28/04/23.
//
import Foundation
import Firebase
import FirebaseStorage


func getAllGroups(completion: @escaping ([sportGroup]) -> Void){
    
    let groupsRef = Database.database().reference().child("groups")
    var groupList: [sportGroup] = []
    let decoder = JSONDecoder()
    
    groupsRef.observe(.childAdded){ snapshot in
        guard
            let json = snapshot.value as? [String: AnyObject]
        else{
            return
        }
        do{
            
            let groupData = try JSONSerialization.data(withJSONObject: json)
            let group = try decoder.decode(sportGroup.self, from: groupData)
            groupList.append(group)
        }catch let error{
            print("Group Error", String(describing: error))
        }
        completion(groupList)
    }
}


func getAllSports(completion: @escaping ([Sport]) -> Void){
    
    let sportsRef = Database.database().reference().child("sports")
    
    var sportsList: [Sport] = []
    let decoder = JSONDecoder()
    
    sportsRef.observe(.childAdded){ snapshot in
        guard
            let json = snapshot.value as? [String: AnyObject]
        else{
            return
        }
        do{
            let sportData = try JSONSerialization.data(withJSONObject: json)
            let sport = try decoder.decode(Sport.self, from: sportData)
            sportsList.append(sport)
        }catch let error{
            print(String(describing: error))
        }
        completion(sportsList)
    }
    
}

func getAllEvents(completion: @escaping ([Event]) -> Void){
    let ref = Database.database().reference().child("events")
    var events: [Event] = []
    let decoder = JSONDecoder()
    
    ref.observe(.childAdded){snapshot in
        guard
            let json = snapshot.value as? [String: AnyObject]
                
        else {
            return
        }
        
        do{
             let eventos = try JSONSerialization.data(withJSONObject: json)
             let event = try decoder.decode(Event.self, from: eventos)
            
              events.append(event)
            
        }catch let error{
             print(String(describing: error))
        }
        completion(events)
}
    
func readValue(){
    

    }
}

func uploadImageToFireBase(_ eventImage: UIImage?,_ idEvent: UUID, completion: @escaping (String?) -> Void) {
    
    let storageRef = Storage.storage().reference()
    
    let fileRef = storageRef.child("events/\(idEvent.uuidString).jpg")
    let imageData = eventImage?.jpegData(compressionQuality: 0.8)
    
    guard imageData != nil else{
        return
    }
    
    _ = fileRef.putData(imageData!, metadata: nil){metadata, error in
        guard let metadata = metadata else{
            debugPrint("ErrorUploadEventImage",error?.localizedDescription ?? "")
            completion(nil)
            return
        }
        
        _ = metadata.size
        
        fileRef.downloadURL(){ (url, error) in
            guard let downloadUrlImage = url else {
                debugPrint("errorDownloadUrlImage:",error?.localizedDescription ?? "")
                completion(nil)
              return
            }
            completion(downloadUrlImage.description)
        }
    }
}

func writeNewGroupDataBase(_ idGroup: UUID, _ name: String, _ description: String, _ img: String, completion: @escaping(String?) -> Void) {
    
    let groupsRef = Database.database().reference().child("groups")
    let creator = Auth.auth().currentUser?.uid ?? "none"
    let id = idGroup.uuidString
    
    do {
        let newGroup = [
            "id": id,
            "description": description,
            "idAmin": creator,
            "image": img,
            "isActive": true,
            "members": [creator],
            "name": name
        ] as [String: Any]
        
        groupsRef.child(id).setValue(newGroup){ (error: Error?, ref:DatabaseReference) in
            if let error = error{
                debugPrint("Group could not be saved: \(error).")
                completion(nil)
            }else{
                debugPrint("Data saved successfully!")
                completion("Success")
            }
        }
    }
    
}

func writeNewEventDataBase<T: ModelData>(_ modelData: T,_ idEvent: UUID, completion: @escaping (String?) -> Void) {
    let eventsRef = Database.database().reference().child("events")
    do {
        let formatterHour = DateFormatter()
        formatterHour.dateFormat = "h:mm a"
        formatterHour.amSymbol = "AM"
        formatterHour.pmSymbol = "PM"
        let hour = formatterHour.string(from: modelData.hourEvent)
        
        let formatterDate = DateFormatter()
        formatterDate.dateFormat = "dd/MM/yyyy"
        let date = formatterDate.string(from: modelData.dateEvent)
        
        let sports = modelData.selectedSports.map {$0.name}
        let userId = Auth.auth().currentUser?.uid
        
        let newEvent = [
            "idUser": userId ?? "",
            "name": modelData.eventName,
            "latitude": modelData.latitude,
            "longitude": modelData.longitude,
            "description": modelData.descriptionEvent,
            "date": date,
            "startTime": hour,
            "endTime": "",
            "sports": sports,
            "img": modelData.imageUrl
        ] as [String : Any]
        
        
        eventsRef.child(idEvent.uuidString).setValue(newEvent){ (error: Error?, ref:DatabaseReference) in
            if let error = error{
                debugPrint("Data could not be saved: \(error).")
                completion(nil)
            }else{
                debugPrint("Data saved successfully!")
                completion("Success")
            }
        }
    }catch let error {
        debugPrint("ErrorCreateEvent",error.localizedDescription)
    }
}
