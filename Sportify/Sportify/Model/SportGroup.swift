//
//  Group.swift
//  Sportify
//
//  Created by Diegoe012 on 20/05/23.
//

import Foundation

struct sportGroup: Hashable, Codable, Identifiable{
    var id: String
    var name: String
    var description: String
    var isActive: Bool
    var idAdmin: String?
    var image: String
    var members: [String]?
}
