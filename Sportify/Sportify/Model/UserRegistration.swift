//
//  UserRegistration.swift
//  Sportify
//
//  Created by Diegoe012 on 23/05/23.
//

import Foundation
import UIKit

struct UserRegistration{
    var name: String?
    var bio: String?
    var birthDate: String?
    var avatar: String?
    var email: String?
    var phoneNumber: Int?
    var password: String?
    var image: UIImage?
}


extension UserRegistration {
    static var new: UserRegistration{
        UserRegistration(name: "", bio: "", birthDate: "", avatar: "", email: "", phoneNumber: 0, password: "", image: UIImage())
    }
}
