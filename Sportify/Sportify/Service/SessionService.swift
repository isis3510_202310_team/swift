//
//  SessionService.swift
//  Sportify
//
//  Created by Diegoe012 on 23/05/23.
//

import Foundation
import Combine
import FirebaseAuth
import Firebase
import FirebaseDatabase


enum SessionState {
    case loggedIn
    case loggedOut
}

protocol SessionService {
    var state: SessionState { get }
    var userDetails: UserDetails? { get }
    init()
    func logout()
}

final class SessionServiceImpl: ObservableObject, SessionService {
    @Published var state: SessionState = .loggedOut
    @Published var userDetails: UserDetails?
    
    private var handler: AuthStateDidChangeListenerHandle?
    
    init(){
        setupFirebaseAuthHandler()
    }
    
    func logout() {
        try? Auth.auth().signOut()
    }
}

private extension SessionServiceImpl {
    func setupFirebaseAuthHandler() {
        handler = Auth
            .auth()
            .addStateDidChangeListener {[weak self] res, user in
                guard let self = self else { return }
                self.state = user == nil ? .loggedOut : .loggedIn
                if let uid = user?.uid{
                    self.handleRefresh(with: uid)
                }
            }
    }
    
    func handleRefresh(with uid: String){
        Database
            .database()
            .reference()
            .child("users")
            .child(uid)
            .observe(.value) { [weak self ] snapshot in
                guard let self = self,
                      let value = snapshot.value as? NSDictionary,
                      let name = value[RegistrationKeys.fullName.rawValue] as? String,
                      let bio = value[RegistrationKeys.bio.rawValue] as? String,
                      let birthDate = value[RegistrationKeys.birthDate.rawValue] as? String,
                      let avatar = value[RegistrationKeys.avatar.rawValue] as? String,
                      let email = value[RegistrationKeys.email.rawValue] as? String,
                      let phone = value[RegistrationKeys.phone.rawValue] as? Int,
                      let interests = value[RegistrationKeys.interests.rawValue] as? [String]
                        
                       else {
                    return
                }
                
                DispatchQueue.main.async {
                    self.userDetails = UserDetails(name: name, bio: bio, birthDate: birthDate ,email: email, avatar: avatar, phone: phone , interests: interests )
                }
            }
    }
}
