//
//  RegistrationService.swift
//  Sportify
//
//  Created by Diegoe012 on 22/05/23.
//

import Combine
import Foundation
import Firebase

enum RegistrationKeys: String {
    case fullName
    case email
    case birthDate
    case phone
    case bio
    case avatar
    case interests
}

protocol RegistrationService{
    func register(with details: UserRegistration) -> AnyPublisher<Void,Error>
}

final class RegistrationServiceImpl: RegistrationService{
    func register(with details: UserRegistration) -> AnyPublisher<Void, Error> {
        Deferred{
            Future { promise in
                Auth.auth().createUser(withEmail: details.email ?? "Default email" , password: details.password ?? ""){ res, error in
                    if let err = error {
                        promise(.failure(err))
                    }else{
                        if let uid = res?.user.uid{
                            let repo: RepositoryInterface = Repository()
                            Task {
                                repo.uploadImageUser(details.image, uid){ downloadUrlImage in
                                    guard let imageUrl = downloadUrlImage else {
                                        return
                                    }
                                    let sports: [String] = ["futbol"]
                                    let values = [
                                        RegistrationKeys.fullName.rawValue: details.name ?? "Default name",
                                        RegistrationKeys.email.rawValue: details.email ?? "Default email",
                                        RegistrationKeys.birthDate.rawValue: details.birthDate ?? "21/11/2001",
                                        RegistrationKeys.phone.rawValue: details.phoneNumber ?? 00000,
                                        RegistrationKeys.bio.rawValue: details.bio ?? "defaultBio",
                                        RegistrationKeys.avatar.rawValue: imageUrl,
                                        RegistrationKeys.interests.rawValue: sports,
                                    ] as [String: Any]
                                    
                                    Database.database()
                                        .reference()
                                        .child("users")
                                        .child(uid)
                                        .updateChildValues(values){ error, ref in
                                            if let err = error {
                                                debugPrint("errorCreate")
                                                promise(.failure(err))
                                            }else{
                                                debugPrint("User Created")
                                                promise(.success(()))
                                            }
                                        }
                                }
                            }
                            
                                                        
                        
                        }else{
                            promise(.failure(NSError(domain: "Invalid User Id", code: 0, userInfo: nil)))
                        }
                    }
                }
            }
        }
        .receive(on: RunLoop.main)
        .eraseToAnyPublisher()
    }
}

