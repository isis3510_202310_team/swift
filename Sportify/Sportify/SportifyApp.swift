//
//  SportifyApp.swift
//  Sportify
//
//  Created by Andres Felipe Delgado Ruiz on 23/03/23.
//

import SwiftUI

import FirebaseCore
import Firebase


class AppDelegate: NSObject, UIApplicationDelegate {
    
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
      FirebaseApp.configure()
      Database.database().isPersistenceEnabled = true
      return true
  }
}

@main
struct SportifyApp: App {
    
    // register app delegate for Firebase setup
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    @StateObject var networkMonitor = NetworkMonitor()
    @StateObject var sessionService = SessionServiceImpl()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(sessionService)
                .environmentObject(networkMonitor)

        }
    }
}
