//
//  EventDAO.swift
//  Sportify
//
//  Created by Lina Sierra on 25/03/23.
//

import Foundation
import Firebase

protocol DAO {
    associatedtype T
    
    func getAll() -> [T]
    func get(byId id: String) -> T?
    func add(_ object: T)
    func update(_ object: T)
    func delete(_ object: T)
}

struct EventDAO {
    var beginTime: String
    var date: String
    var details: String
    var endTime: String
    var img: String
    var name: String
    var place: String
    var sports: [Any] = []
    
}

