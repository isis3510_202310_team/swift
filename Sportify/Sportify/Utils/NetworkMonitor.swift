//
//  ConnectionState.swift
//  Sportify
//
//  Created by MacBookPro on 28/04/23.
//

import Foundation
import Network
import Firebase

final class NetworkMonitor: ObservableObject{
    
    static let shared = NetworkMonitor()
    let monitor : NWPathMonitor

    @Published var isConnected: Bool = false
    @Published var isConnectedFireBase: Bool = false
    @Published var connectionType: ConnectionType? = .unknow

    enum ConnectionType {
        case wifi
        case cellular
        case ethernet
        case unknow
    }
    
    init(){
        monitor = NWPathMonitor()
        Task{
            startMonitoring()
            await MainActor.run {
                self.objectWillChange.send()
            }
        }
    }

    public func startMonitoring(){
        monitor.start(queue: DispatchQueue.global())
        monitor.pathUpdateHandler = { [weak self] path in
            DispatchQueue.main.async {
                self?.isConnected = path.status == .satisfied
                self?.getConnectionType(path)
                self?.getFirebaseConnection{connect in
                    print("ConnectedIsFirebase:",connect)
                    self?.isConnectedFireBase = connect
                }
                
                
                
                print("ConnectedIs: ",self?.isConnected ?? "")
                print("type: ", self?.connectionType ?? "")
            }
        }
    }

    public func stopMonitoring(){
        monitor.cancel()
    }


    private func getConnectionType(_ path: NWPath){
        if path.usesInterfaceType(.wifi){
            connectionType = .wifi
        }else if path.usesInterfaceType(.cellular){
            connectionType = .cellular
        }else if path.usesInterfaceType(.wiredEthernet){
            connectionType = .ethernet
        }else{
            connectionType = .unknow
        }
    }
    
    private func getFirebaseConnection(completion: @escaping (Bool) -> Void ) {
        let connectedRef = Database.database().reference(withPath: ".info/connected")
        connectedRef.observe(.value, with: { snapshot in
            if snapshot.value as? Bool ?? false{
                completion(true)
            }else{
                completion(false)
            }
        })
    }
}
