//
//  ImageCache.swift
//  Sportify
//
//  Created by Diegoe012 on 21/05/23.
//

import Foundation

class ImageCache{
    
    typealias CacheType = NSCache<NSString, NSData>
    
    static let shared = ImageCache()
    
    private init(){
        
    }
    
    private lazy var cache: CacheType = {
        let cache = CacheType()
        cache.countLimit = 100
        cache.totalCostLimit = 50 * 1024 * 1024 // aprox 50MB
        return cache
    }()
    
    func object(forkey key: NSString) -> Data? {
        return cache.object(forKey: key) as Data?
    }
    
    func set(object: NSData, forkey key: NSString){
        cache.setObject(object, forKey: key)
    }
}
