//
//  FileStorageManager.swift
//  Sportify
//
//  Created by Diegoe012 on 24/05/23.
//

import Foundation

final class FileStorageManager {
    static let shared = FileStorageManager()
    private let fileManager = FileManager.default
    private var cacheFolderURL: URL? {
        fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first
    }
    
    private init(){
        
    }
    
    func retrive(with fileName: String) -> Item? {
        guard let cacheFolder = cacheFolderURL else { return nil }
        let fileURL = cacheFolder.appendingPathComponent(fileName + ".cache")
        
        guard let data = try? Data(contentsOf: fileURL),
            let item = try? JSONDecoder().decode(Item.self, from: data) else {
            debugPrint("Failed to get file \(fileName) from disk")
            return nil
        }
        
        debugPrint("Succesfully got file \(fileName) from disk")
        return item
    }
    
    func save(_ item: Item){
        guard let cacheFolder = cacheFolderURL else { return }
        let fileURL = cacheFolder.appendingPathComponent(item.name + ".cache")
        
        debugPrint("Creating path for file -> \(fileURL.absoluteString)")
        
        do {
            let data = try JSONEncoder().encode(item)
            try data.write(to: fileURL)
            debugPrint("Saved item to disk with name \(item.name)")
        } catch{
            debugPrint("FileManagerError:", error)
        }
        
    }
}

extension FileStorageManager {
    struct Item: Codable {
        let name: String
        let data: Data
        
        init(name: String, data: Data) {
            self.name = name.trimmingCharacters(in: .whitespacesAndNewlines)
            self.data = data
        }
    }
}
