//
//  CachedImageManager.swift
//  Sportify
//
//  Created by Diegoe012 on 21/05/23.
//

import Foundation

final class CachedImageManager: ObservableObject{
    
    @Published private(set) var currentState: CurrentState?
    
    private let imageRetriever = ImageRetriever()
    
    @MainActor
    func load(_ item: (name: String, url: String), cache: ImageCache = .shared) async{
        
        if let imageData = cache.object(forkey: item.url as NSString){
            self.currentState = .success(data: imageData)
            debugPrint("Fetching image from the cache: \(item.url)")
            return
        }
        
        if let diskCacheItem = FileStorageManager.shared.retrive(with: item.name){
            
            cache.set(object: diskCacheItem.data as NSData, forkey: diskCacheItem.name as NSString)
            
            self.currentState = .success(data: diskCacheItem.data)
            return
        }
        
        self.currentState = .loading
        
        
        
        do{
            let data = try await imageRetriever.fetch(item.url)
            self.currentState = .success(data: data)
            cache.set(object: data as NSData, forkey: item.name as NSString)
            FileStorageManager.shared.save(.init(name: item.name, data: data))
            debugPrint("Caching image: \(item.url)")
        }catch{
            debugPrint("Error Cached Manager",error)
            self.currentState = .failed(error: error)
        }
    }
}

extension CachedImageManager{
    enum CurrentState {
        case loading
        case failed(error: Error)
        case success(data: Data)
    }
}

extension CachedImageManager.CurrentState: Equatable {
    static func == (lhs: CachedImageManager.CurrentState, rhs: CachedImageManager.CurrentState) -> Bool{
        switch (lhs, rhs) {
        case (.loading, .loading):
            return true
        case (let .failed(lhsError), let .failed(rhsError)):
            return lhsError.localizedDescription == rhsError.localizedDescription
        case (let .success(lhsData), let .success(rhsData)):
            return lhsData == rhsData
        default:
            return false
        }
    }
}


