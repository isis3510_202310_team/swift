//
//  MapItemModel.swift
//  Sportify
//
//  Created by MacBookPro on 7/04/23.
//

import Foundation
import CoreLocation
import MapKit

final class MapItemViewModel: NSObject, ObservableObject, CLLocationManagerDelegate{
    
    
    @Published var region: MKCoordinateRegion = MKCoordinateRegion(
        center: CLLocationCoordinate2D(
            latitude: MapDefaults.latitude,
            longitude: MapDefaults.longitude),
        span: MKCoordinateSpan(
            latitudeDelta: MapDefaults.zoom,
            longitudeDelta: MapDefaults.zoom))

        
    private enum MapDefaults {
//      Uniandes default coordinates
        static let latitude = 4.603166
        static let longitude = -74.065112
        static let zoom = 0.005
    }
    
    var locationManager : CLLocationManager?
    
    func checkIfLocationServicesIsEnabled(){
        if CLLocationManager.locationServicesEnabled(){
            locationManager = CLLocationManager()
            locationManager!.delegate = self
        }
        else{
            print("Locations is not enabled")
        }
    }
    
    func checkLocationAuthorization(){
        guard let locationManager = locationManager else { return }
        switch locationManager.authorizationStatus{
            
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            print("Your location is Restricted")
        case .denied:
            print("Your location is Denied")
        case .authorizedAlways, .authorizedWhenInUse:
            print("Set new Coordinate")
            print(locationManager.location!.coordinate)
            DispatchQueue.main.async {
                self.region  = MKCoordinateRegion(
                    center: locationManager.location!.coordinate,
                    span: MKCoordinateSpan(latitudeDelta: MapDefaults.zoom, longitudeDelta: MapDefaults.zoom))
            }
            
        @unknown default:
            break
        }
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
}
