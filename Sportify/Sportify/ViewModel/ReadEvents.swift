//
//  ReadEventsView.swift
//  Sportify
//
//  Created by Lina Sierra on 21/04/23.
//

import SwiftUI
import Firebase
import FirebaseDatabase
import FirebaseDatabaseSwift
import Foundation

class ReadEvents:ObservableObject{
    
    @Published var events: [Event] = []
    @Published var eventsFilter: [Event] = []
    
    private let repo: RepositoryInterface = Repository()
    

    init(){
        self.repo.getAllEventsFromDataBase(){events in
            self.events = events
        }
    }
    
    
    //cuando se filtren se usa en la vista events filter y cuando se quite vuelve a mostrarse events
    func filterEvents(tipo: String) {
        var eventosActuales = events
        var eventosFiltrados: [Event] = []
        if tipo == "week" {
            //print("WEEK")
            for e in eventosActuales {
                if isDateInCurrentWeek(dateString: e.date!) {
                    eventosFiltrados.append(e)
                }
            }
        }
        else if tipo == "month" {
            //print("MONTH")
            for e in eventosActuales {
                if isDateInCurrentMonth(dateString: e.date!) {
                    eventosFiltrados.append(e)
                }
            }
            
        }
        
        eventsFilter = eventosFiltrados
        
        print("eventosFiltrados", eventsFilter)
    }
    func isDateInCurrentWeek(dateString: String) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        guard let date = dateFormatter.date(from: dateString) else {
            return false
        }
        
        let calendar = Calendar.current
        let currentWeek = calendar.component(.weekOfYear, from: Date())
        let targetWeek = calendar.component(.weekOfYear, from: date)
        
        return currentWeek == targetWeek
    }
    
    func isDateInCurrentMonth(dateString: String) -> Bool {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        guard let date = formatter.date(from: dateString) else {
            return false
        }
        
        let calendar = Calendar.current
        let currentMonth = calendar.component(.month, from: Date())
        let month = calendar.component(.month, from: date)
        
        return currentMonth == month
    }

}
