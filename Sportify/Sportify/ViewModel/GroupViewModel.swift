//
//  GroupViewModel.swift
//  Sportify
//
//  Created by Diegoe012 on 21/05/23.
//

import Foundation

final class GroupViewModel: ObservableObject{
    
    @Published var groupList: [sportGroup] = []
    private let repo: RepositoryInterface = Repository()
    
    init(){
        self.repo.getAllGroupsFromDataBase(){ groups in
            self.groupList = groups
        }
    }
}
