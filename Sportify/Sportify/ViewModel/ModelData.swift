//
//  ModelData.swift
//  Sportify
//
//  Created by MacBookPro on 25/03/23.
//

import Foundation
import SwiftUI
import CoreTransferable

final class ModelData: ObservableObject{
    
    // MARK: - Event Details
    @Published var sports: [Sport] = []
    @Published var eventName: String = ""
    @Published var selectedSports: Set<Sport> =  Set<Sport>()
    @Published var dateEvent: Date = Date()
    @Published var hourEvent: Date = Date()
    @Published var descriptionEvent: String = ""
    @Published var latitude: Float = 0
    @Published var longitude: Float = 0
    @Published var imageUrl: String = ""
    
    // MARK: - Event Image
    enum ImageState{
        case empty
        case loading(Progress)
        case success(Image)
        case failure(Error)
    }
    
    func clean(){
        self.sports = []
        self.eventName = ""
        self.selectedSports = Set<Sport>()
        self.dateEvent = Date()
        self.hourEvent = Date()
        self.descriptionEvent = ""
        self.latitude = 0
        self.longitude = 0
        self.imageUrl = ""
    }
    
}
