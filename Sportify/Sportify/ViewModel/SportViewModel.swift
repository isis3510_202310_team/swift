//
//  SportViewModel.swift
//  Sportify
//
//  Created by MacBookPro on 21/04/23.
//

import Foundation

final class SportViewModel: ObservableObject{
    
    @Published var sportsList: [Sport] = []
    
    private let repo: RepositoryInterface = Repository()

    
    init() {
        self.repo.getAllSportsFromDataBase(){sports in
//            debugPrint("sports:",sports)
            self.sportsList = sports
        }
    }

}


